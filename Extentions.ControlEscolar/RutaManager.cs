﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;

namespace Extentions.ControlEscolar
{
    public class RutaManager
    {
        private string _appPath;
        private const string LOGOS = "logos"; //Las constantes van en mayusculas

        public RutaManager(string appPath)
        {
            _appPath = appPath;
        }
        public string RutaRepositorioLogos
        {
            get { return Path.Combine(_appPath, LOGOS); }
        }
        public void CrearRepositoriosLogos()
        {
            if (!Directory.Exists(RutaRepositorioLogos))
            {
                Directory.CreateDirectory(RutaRepositorioLogos);
            }
        }
        public void CrearRepositorioLogoEscuela(int escuelaId)
        {
            CrearRepositoriosLogos();
            string ruta = Path.Combine(RutaRepositorioLogos, escuelaId.ToString());
            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }
        }
        public string  RutaLogoEscuela(Escuela2 escuela2)
        {
            return Path.Combine(RutaRepositorioLogos, escuela2.Idescuela.ToString(), escuela2.Logo);
        }
    }
}
