CREATE TABLE `alumnos` (
  `numero_control` varchar(10) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidoPaterno` varchar(50) DEFAULT NULL,
  `apellidoMaterno` varchar(50) DEFAULT NULL,
  `sexo` varchar(50) DEFAULT NULL,
  `fechaNacimiento` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `telefefono` varchar(50) DEFAULT NULL,
  `fkestado` varchar(50) DEFAULT NULL,
  `fkmunicipio` varchar(50) DEFAULT NULL,
  `domicilio` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`numero_control`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `escuela` (
  `NombreEscuela` varchar(50) DEFAULT NULL,
  `DomicilioEscuela` varchar(100) DEFAULT NULL,
  `NumeroExterior` int(11) DEFAULT NULL,
  `Estado` varchar(50) DEFAULT NULL,
  `Municipio` varchar(50) DEFAULT NULL,
  `Telefono` varchar(50) DEFAULT NULL,
  `Pagina_Web` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `director` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `escuela2` (
  `idescuela` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `director` varchar(50) DEFAULT NULL,
  `logo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idescuela`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `estado` (
  `codigo` varchar(3) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `maestros` (
  `numero_control` varchar(10) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `apellido_paterno` varchar(50) DEFAULT NULL,
  `apellido_materno` varchar(50) DEFAULT NULL,
  `fecha_nacimiento` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  `sexo` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `tarjeta` varchar(50) DEFAULT NULL,
  `nivel_ing_lic` varchar(200) DEFAULT NULL,
  `nivel_maestria` varchar(50) DEFAULT NULL,
  `nivel_doctorado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`numero_control`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `materias` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Materia` varchar(50) DEFAULT NULL,
  `fk_Anterior` int(11) DEFAULT NULL,
  `fk_Posterior` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_Anterior` (`fk_Anterior`),
  KEY `fk_Posterior` (`fk_Posterior`),
  CONSTRAINT `materias_ibfk_1` FOREIGN KEY (`fk_Anterior`) REFERENCES `materias` (`Id`),
  CONSTRAINT `materias_ibfk_2` FOREIGN KEY (`fk_Posterior`) REFERENCES `materias` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

CREATE TABLE `municipio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `fk_estado` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_estado` (`fk_estado`),
  CONSTRAINT `municipio_ibfk_1` FOREIGN KEY (`fk_estado`) REFERENCES `estado` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=2364 DEFAULT CHARSET=latin1;

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `apellidopaterno` varchar(50) DEFAULT NULL,
  `apellidomaterno` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
