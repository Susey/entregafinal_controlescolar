﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.Controlescolar;
using System.Text.RegularExpressions;
//using AccesoDatos.Controlescolar;
using Entidades.Controlescolar;

namespace LogicaNegocio.Controlescolar
{
    public class AlumnosManejador
    {
        private AlumnosAccesoDatos _alumnoAccesoDatos;

        public AlumnosManejador()
        {
            _alumnoAccesoDatos = new AlumnosAccesoDatos();
        }
        public void Eliminar(string numero_control)
        {
            _alumnoAccesoDatos.Eliminar(numero_control);
        }

        public void Guardar(Alumnos alumno)
        {
            _alumnoAccesoDatos.Guardar(alumno);
        }

        public List<Alumnos> ObtenerLista(string filtro)
        {
            var list = new List<Alumnos>();
            list = _alumnoAccesoDatos.ObtenerLista(filtro);
            return list;
        }

        private bool NommbreValidoA(string nombre)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        private bool ApellidoPaternoValidoA(string apellidopaterno)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$");
            var match = regex.Match(apellidopaterno);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        private bool ApellidoMaternoValidoA(string apellidomaterno)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$");
            var match = regex.Match(apellidomaterno);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool Domicilio(string domicilio)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$");
            var match = regex.Match(domicilio);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        private bool Correo(string correo)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$");
            var match = regex.Match(correo);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        private bool Telefono(string telefono)
        {
            var regex = new Regex(@"^[A-Z]*$|\s$");
            var match = regex.Match(telefono);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public Tuple<bool, string> EsusuarioValidoA(Alumnos alumno)
        {
            string mensaje = " ";
            bool valido = true;

            if (alumno.Nombre.Length == 0)
            {
                mensaje = "El nombre de alumno es necesario";
                valido = false;
            }
            else if (NommbreValidoA(alumno.Nombre))
            {
                mensaje = "Escribre un formato valido para el nombre";
                valido = false;
            }
            else if (alumno.Nombre.Length > 100)
            {
                mensaje = "La longitud para nombre de alumno es maximo de 100 caracteres";
                valido = false;
            }
            else if (alumno.ApellidoPaterno.Length == 0)
            {
                mensaje = "El apellido paterno de alumno es necesario";
                valido = false;
            }
            else if (ApellidoPaternoValidoA(alumno.ApellidoPaterno))
            {
                mensaje = "Escribe un formato valido para el apellido paterno";
                valido = false;
            }
            else if (alumno.ApellidoPaterno.Length > 100)
            {
                mensaje = "La longitud para el apellido paterno de alumno es maximo de 100 caracteres";
                valido = false;
            }
            else if (alumno.ApellidoMaterno.Length == 0)
            {
                mensaje = "El apellido materno de alumno es necesario";
                valido = false;
            }
            else if (ApellidoMaternoValidoA(alumno.ApellidoMaterno))
            {
                mensaje = "Escribe un formato valido para el apellido materno";
                valido = false;
            }
            else if (alumno.ApellidoMaterno.Length > 100)
            {
                mensaje = "La longitud para el apellido materno de alumno es maximo de 100 caracteres";
                valido = false;
            }
            else if (alumno.Domicilio.Length > 250)
            {
                mensaje = "La longitud para el domicilio de alumno es maximo de 250 caracteres campo opcional";
                valido = false;
            }
            else if (Correo(alumno.Correo))
            {
                mensaje = "Solo caracteres de correo electronico para la validacion";
                valido = false;
            }
            else if (alumno.Correo.Length > 250)
            {
                mensaje = "La longitud para el correo de alumno es maximo de 250 caracteres campo opcional";
                valido = false;
            }
            else if (Telefono(alumno.Telefefono))
            {
                mensaje = "Solo caracteres de numero campo opcional";
                valido = false;
            }
            else if (alumno.Telefefono.Length > 15)
            {
                mensaje = "La longitud para el telefono de alumno es maximo de 15 caracteres";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }
    }

}

