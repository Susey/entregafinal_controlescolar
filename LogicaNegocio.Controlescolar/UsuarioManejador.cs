﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using AccesoDatos.Controlescolar;
using System.Text.RegularExpressions;


namespace LogicaNegocio.Controlescolar
{
    public class UsuarioManejador //Se le puede agregar la Interfaz
    {
        private UsuariosAccesoDatos _usuariosAccesoDatos;
        public UsuarioManejador()
        {
            _usuariosAccesoDatos = new UsuariosAccesoDatos();
        }
        public void Eliminar(int idUsuario)
        {
            _usuariosAccesoDatos.Eliminar(idUsuario);
        }

        public void Guardar(Usuarios usuario)
        {
            _usuariosAccesoDatos.Guardar(usuario);
        }

        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A Z]*$|\s$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool ApellidoPaternoValido(string ApellidoPaterno)
        {
            var regex = new Regex(@"^[A Z]*$|\s$");
            var match = regex.Match(ApellidoPaterno);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        private bool ApellidoMaternoValido(string ApellidoMaterno)
        {
            var regex = new Regex(@"^[A Z]*$|\s$");
            var match = regex.Match(ApellidoMaterno);

            if (match.Success)
            {
                return true;
            }
            return false;
        }

        public List<Usuarios> ObtenerLista(string filtro)
        {
            var list = new List<Usuarios>();
            list = _usuariosAccesoDatos.ObtenerLista(filtro);
            return list;
        }


          public  Tuple<bool,string> EsusuarioValidoU(Usuarios usuario)
        {
            string mensaje = "";
            bool valido = true;

           if (usuario.Nombre.Length==0)
            {
                mensaje = "El nombre de usuario es necesario";
                valido = false;
            }
           else if (NombreValido(usuario.Nombre))
            {
                mensaje = "Escribe un formato valido para el nombre";
                valido = false;
            }
           else if(usuario.Nombre.Length>15)
            {
                mensaje = "La longitud para nombre de usuario es maximo 15 caracteres";
                valido = false;
            }
            if (usuario.ApellidoPaterno.Length == 0)
            {
                mensaje = "El apellido paterno de usuario es necesario";
                valido = false;
            }
            else if (ApellidoPaternoValido(usuario.ApellidoPaterno))
            {
                mensaje = "Escribe un formato valido para el apellido paterno";
                valido = false;
            }
            else if (usuario.ApellidoPaterno.Length > 15)
            {
                mensaje = "La longitud para nombre de usuario es maximo 15 caracteres";
                valido = false;
            }
            if (usuario.ApellidoMaterno.Length == 0)
            {
                mensaje = "El apellido materno de usuario es necesario";
                valido = false;
            }
            else if (ApellidoMaternoValido(usuario.ApellidoMaterno))
            {
                mensaje = "Escribe un formato valido para el apellido paterno";
                valido = false;
            }
            else if (usuario.ApellidoMaterno.Length > 15)
            {
                mensaje = "La longitud para nombre de usuario es maximo 15 caracteres";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
        

    }
}
