﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.Controlescolar;
using Entidades.Controlescolar;

namespace LogicaNegocio.Controlescolar
{
    public class EstadosManejador
    {
        private EstadosAccesoDatos _estadosAccesoDatos;
        public EstadosManejador()
        {
            _estadosAccesoDatos = new EstadosAccesoDatos();
        }
        public void Eliminar(string Clave)
        {
            _estadosAccesoDatos.Eliminar(Clave);
        }
        public void Guardar(Estados estado)
        {
            _estadosAccesoDatos.Guardar(estado);
        }
        public List<Estados> ObtenerLista(string filtro)
        {
            var list = new List<Estados>();
            list = _estadosAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}
