﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.Controlescolar;
using Entidades.Controlescolar;

namespace LogicaNegocio.Controlescolar
{
    public class MunicipioManejador
    {
        private MunicipiosAccesoDatos _municipiosAccesoDatos;
        public MunicipioManejador()
        {
            _municipiosAccesoDatos = new MunicipiosAccesoDatos();
        }
        public void Eliminar(int Id)
        {
            _municipiosAccesoDatos.Eliminar(Id);
        }
        public void Guardar(Municipio municipio)
        {
            _municipiosAccesoDatos.Guardar(municipio);
        }
        public List<Municipio> ObtenerLista(string filtro)
        {
            var list = new List<Municipio>();
            list = _municipiosAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}
