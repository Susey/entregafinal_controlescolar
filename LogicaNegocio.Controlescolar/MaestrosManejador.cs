﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using AccesoDatos.Controlescolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.Controlescolar
{
    public class MaestrosManejador
    {
        private MaestrosAccesodatos _maestroAccesoDatos;
        public MaestrosManejador()
        {
            _maestroAccesoDatos = new MaestrosAccesodatos();
        }
        public void Eliminar(string Numero_control)
        {
            _maestroAccesoDatos.Eliminar(Numero_control);
        }
        public void Guardar(Maestros maestro)
        {
            _maestroAccesoDatos.Guardar(maestro);
        }
        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A Z]*$|\s$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }


        public List<Maestros> ObtenerLista(string filtro)
        {
            var list = new List<Maestros>();
            list = _maestroAccesoDatos.ObtenerLista(filtro);
            return list;
        }


        private Tuple<bool, string> EsMaestroValido(Maestros maestro)
        {
            string mensaje = "";
            bool valido = true;

            if (maestro.Nombre.Length == 0)
            {
                mensaje = "El nombre de usuario es necesario";
                valido = false;
            }
            else if (maestro.Nombre.Length > 100)
            {
                mensaje = "La longitud para Nombre de maestro es de 100 caracteres";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }
        private Tuple<bool, string> EsAprellidoValido(Maestros maestro)
        {
            string mensaje = "";
            bool valido = true;

            if (maestro.Apellido_paterno.Length == 0)
            {
                mensaje = "El apellido paterno es necesario";
                valido = false;
            }
            else if (maestro.Apellido_paterno.Length > 100)
            {
                mensaje = "La longitud para Apelllido es de 100 caracteres";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }
        private Tuple<bool, string> EsAprellidoMaternoValido(Maestros maestro)
        {
            string mensaje = "";
            bool valido = true;

            if (maestro.Apellido_materno.Length == 0)
            {
                mensaje = "El apellido paterno es necesario";
                valido = false;
            }
            else if (maestro.Apellido_materno.Length > 100)
            {
                mensaje = "La longitud para Apelllido es de 100 caracteres";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }
        private Tuple<bool, string> EsCiudadValido(Maestros maestro)
        {
            string mensaje = "";
            bool valido = true;

            if (maestro.Ciudad.Length!= 0)
            {
                mensaje = "Ciudad no es necesario";
                valido = false;
            }
            else if (NombreValido(maestro.Ciudad))
            {
                mensaje = "Escribe un formato valido para la ciudad";
                valido = false;
            }
            else if (maestro.Ciudad.Length > 100)
            {
                mensaje = "La longitud para ciudad es maximo 250";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }
        private Tuple<bool, string> EsCorreoValido(Maestros maestro)
        {
            string mensaje = "";
            bool valido = true;

            if (maestro.Correo.Length != 0)
            {
                mensaje = "El Correo puede llenarse después";
                valido = false;
            }
            else if (NombreValido(maestro.Correo))
            {
                mensaje = "Escribe un formato valido para el correo";
                valido = false;
            }
            else if (maestro.Correo.Length > 250)
            {
                mensaje = "La longitud para domicilio de alumno es maximo 250";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }
        private Tuple<bool, string> TarjetaValida(Maestros maestro)
        {
            string mensaje = "";
            bool valido = true;
            if (maestro.Tarjeta.Length <= 16)
            {
                mensaje = "La longitud para numero de tarjeta es 16";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }

    }

}