﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;


namespace AccesoDatos.Controlescolar
{
    public class AlumnosAccesoDatos
    {
        Conexion _conexion;
        public AlumnosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "scolar", 3306);
        }
            public void Eliminar(string Numero_control)
            {
                string cadena = string.Format("Delete from alumnos where numero_control={0}", Numero_control);
                _conexion.EjecutarConsulta(cadena);
            }

            public void Guardar(Alumnos alumno)
            {
                if (alumno.Numero_control != "")
                {
                    string cadena = string.Format("Insert into alumnos values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')", alumno.Numero_control, alumno.Nombre, alumno.ApellidoPaterno, alumno.ApellidoMaterno, alumno.Sexo, alumno.Fechanacimiento, alumno.Correo, alumno.Telefefono, alumno.Fkestado, alumno.Fkmunicipio, alumno.Domicilio);
                    _conexion.EjecutarConsulta(cadena);
                }
                else
                {
                string cadena = string.Format("Update alumnos set  nombre= '{0}', apellidoPaterno= '{1}', apellidoMaterno='{2}', sexo='{3}', fechaNacimiento='{4}', correo='{5}', telefefono='{6}', fkestado='{7}', fkmunicipio='{8}', domicilio='{9}' where numero_control='{}'",  alumno.Nombre, alumno.ApellidoPaterno, alumno.ApellidoMaterno, alumno.Sexo, alumno.Fechanacimiento, alumno.Correo, alumno.Telefefono, alumno.Fkestado, alumno.Fkmunicipio, alumno.Domicilio, alumno.Numero_control);
                    _conexion.EjecutarConsulta(cadena);
                }
            }

            public List<Alumnos> ObtenerLista(string filtro)
            {
                var list = new List<Alumnos>();
                string consulta = string.Format("Select * from alumnos where nombre like '%{0}%'", filtro);
                var ds = _conexion.ObtenerDatos(consulta, "alumno");
                var dt = ds.Tables[0]; //Variable data table arreglo de tablas

                foreach (DataRow row in dt.Rows)
                {
                    var alumno = new Alumnos
                    {
                        Numero_control= row["numero_control"].ToString(),
                        Nombre = row["nombre"].ToString(),
                        ApellidoPaterno = row["apellidoPaterno"].ToString(),
                        ApellidoMaterno = row["apellidoMaterno"].ToString(),
                        Sexo = row["sexo"].ToString(),
                        Fechanacimiento = row["fechaNacimiento"].ToString(),
                        Correo = row["correo"].ToString(),
                        Telefefono = row ["telefefono"].ToString(),
                        Fkestado = row["fkestado"].ToString(),
                        Fkmunicipio= row["fkmunicipio"].ToString(),
                        Domicilio=  row["domicilio"].ToString()
                    };

                    list.Add(alumno);
                }

                return list;
            }
        }
    }


