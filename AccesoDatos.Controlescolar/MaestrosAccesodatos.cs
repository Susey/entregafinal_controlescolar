﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
    public class MaestrosAccesodatos
    {
        Conexion _conexion;
        public MaestrosAccesodatos()
        {
            _conexion = new Conexion("localhost", "root", "", "Scolar", 3306);
        }
        public void Eliminar(string numero_control)
        {
            string cadena = string.Format("Delete from maestros where numero_control={0}", numero_control);
            _conexion.EjecutarConsulta(cadena);
        }

        public void Guardar(Maestros maestro)
        {
            if (maestro.Numero_control != "")
            {
                string cadena = string.Format("Insert into maestros values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')", maestro.Numero_control, maestro.Nombre, maestro.Apellido_paterno, maestro.Apellido_materno, maestro.Fecha_nacimiento, maestro.Estado, maestro.Ciudad, maestro.Sexo, maestro.Correo, maestro.Tarjeta, maestro.Nivel_estudio);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update maestros set nombre= '{0}', apellido_paterno= '{1}', apellido_materno= '{2}', fecha_nacimiento ='{3}', estado='{4}', ciudad='{5}', sexo='{6}', correo='{7}', tarjeta='{8}', nivel_estudio ='{9}',  where numero_control='{}'", maestro.Nombre, maestro.Apellido_paterno, maestro.Apellido_materno, maestro.Fecha_nacimiento, maestro.Estado, maestro.Ciudad, maestro.Sexo, maestro.Correo, maestro.Tarjeta, maestro.Nivel_estudio, maestro.Numero_control);
                _conexion.EjecutarConsulta(cadena);
            }


        }

        public List<Maestros> ObtenerLista(string filtro)
        {
            var list = new List<Maestros>();
            string consulta = string.Format("Select * from maestros where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "maestro");
            var dt = ds.Tables[0]; //Variable data table arreglo de tablas

            foreach (DataRow row in dt.Rows)
            {
                var maestro = new Maestros
                {
                    Numero_control = row["numero_control"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Apellido_paterno = row["apellido_paterno"].ToString(),
                    Apellido_materno = row["apellido_materno"].ToString(),
                    Fecha_nacimiento = row["fecha_nacimiento"].ToString(),
                    Estado = row["estado"].ToString(),
                    Ciudad = row["ciudad"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Correo = row["correo"].ToString(),
                    Tarjeta = row["tarjeta"].ToString(),
                    Nivel_estudio = row["nivel_estudio"].ToString()
                };

                list.Add(maestro);
            }

            return list;
        }
    }
}
