﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
    public class Escuela2AccesoDatos
    {
        Conexion _conexion;
        public Escuela2AccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "scolar", 3306);
        }
        public void Eliminar(string Idescuela)
        {
            string cadena = string.Format("Delete from escuela2 where IdEscuela={0}", Idescuela);
            _conexion.EjecutarConsulta(cadena);
        }
        public void Guardar(Escuela2 escuela2)
        {
            if (escuela2.Idescuela == 0)
            {
                string cadena = string.Format("Insert into escuela2 values('{0}','{1}','{2}')", escuela2.Nombre, escuela2.Director, escuela2.Logo);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update escuela2 set nombre= '{0}', director= '{1}', logo='{2}' where idescuela='{}'", escuela2.Nombre, escuela2.Director, escuela2.Logo, escuela2.Idescuela);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public Escuela2 GetEscuela2()
        {
            var ds = new DataSet();
            string consulta = String.Format("Select * from escuela2");
            ds = _conexion.ObtenerDatos(consulta, "escuela2");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var escuela2 = new Escuela2();
                foreach (DataRow row in dt.Rows)
                {
                    escuela2.Idescuela = Convert.ToInt32(row["IdEscuela"]);
                    escuela2.Nombre = row["nombre"].ToString();
                    escuela2.Director = row["director"].ToString();
                    escuela2.Logo = row["logo"].ToString();

                }
                return escuela2;
         }
    }
}

