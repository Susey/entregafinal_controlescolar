﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
    public class EscuelaAccesoDatos
    {
        Conexion _conexion;
        public EscuelaAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "Scolar", 3306);
        }
        public void Eliminar(string NombreEscuela)
        {
            string cadena = string.Format("Delete from escuela where Nombreescuela={0}", NombreEscuela);
            _conexion.EjecutarConsulta(cadena);
        }
        public void Guardar(Escuela escuela)
        {
            if (escuela.NombreEscuela == " ")
            {
                string cadena = string.Format("Insert into escuela values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", escuela.NombreEscuela, escuela.DomicilioEscuela, escuela.NumeroExterior, escuela.Estado, escuela.Municipio, escuela.Telefono, escuela.Pagina_Web, escuela.Email, escuela.Director);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update escuela set DomicilioEscuela= '{0}', NumeroExterior= '{1}', Estado='{2}', Municipio='{3}', Telefono='{4}', Pagina_Web='{5}', email='{6}', director='{7}', where NombreEscuela='{}'", escuela.DomicilioEscuela, escuela.NumeroExterior, escuela.Estado, escuela.Municipio, escuela.Telefono, escuela.Pagina_Web, escuela.Email, escuela.Director, escuela.NombreEscuela);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<Escuela> ObtenerLista(string filtro)
        {
            var list = new List<Escuela>();
            string consulta = string.Format("Select * from escuela where NombreEscuela like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "escuela");
            var dt = ds.Tables[0]; //Variable data table arreglo de tablas

            foreach (DataRow row in dt.Rows)
            {
                var escuela = new Escuela
                {
                    NombreEscuela = row["NombreEscuela"].ToString(),
                    DomicilioEscuela = row["DomicilioEscuela"].ToString(),
                    NumeroExterior = row["NumeroExterior"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Municipio = row["Municipio"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Pagina_Web = row["Pagina_Web"].ToString(),
                    Email = row["email"].ToString(),
                    Director = row["director"].ToString(),
                };
                ;

                list.Add(escuela);
            }


            return list;
        }
        
    }
}
