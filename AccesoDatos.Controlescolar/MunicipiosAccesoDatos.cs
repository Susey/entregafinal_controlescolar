﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
    public class MunicipiosAccesoDatos
    {
        Conexion _conexion;
        public MunicipiosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "Scolar", 3306);
        }
        public void Eliminar(int Id)
        {
            string cadena = string.Format("Delete from municipio where id={0}", Id);
            _conexion.EjecutarConsulta(cadena);
        }
        public void Guardar(Municipio municipio)
        {
            if (municipio.Id == 0)
            {
                string cadena = string.Format("Insert into municipio values(null, '{0}','{1}')", municipio.Id, municipio.Nombre, municipio.Fk_estado);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update municipio set nombre= '{0}', fk_estado='{1} where id={}'", municipio.Nombre, municipio.Fk_estado, municipio.Id);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<Municipio> ObtenerLista(string filtro)
        {
            var list = new List<Municipio>();
            string consulta = string.Format("Select * from municipio where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "municipio");
            var dt = ds.Tables[0]; //Variable data table arreglo de tablas

            foreach (DataRow row in dt.Rows)
            {
                var municipio = new Municipio
                {
                    Id = Convert.ToInt32(row["Id"]),
                    Nombre = row["nombre"].ToString(),
                    Fk_estado = row["fk_estado"].ToString(),

                };

                list.Add(municipio);
            }

            return list;
        }
    }
}
