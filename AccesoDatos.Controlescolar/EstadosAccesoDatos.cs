﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.Controlescolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.Controlescolar
{
    public class EstadosAccesoDatos
    {
        Conexion _conexion;
        public EstadosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "Scolar", 3306);
        }
        public void Eliminar(string clave)
        {
            string cadena = string.Format("Delete from estado where clave={0}", clave);
            _conexion.EjecutarConsulta(cadena);
        }

        public void Guardar(Estados estado)
        {
            if (estado.Codigo != null)
            {
                string cadena = string.Format("Insert into estado values('{0}','{1}')", estado.Codigo, estado.Nombre);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = string.Format("Update estado set nombre= '{0}', where codigo='{1}'", estado.Nombre, estado.Codigo);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public List<Estados> ObtenerLista(string filtro)
        {
            var list = new List<Estados>();
            string consulta = string.Format("Select * from estado where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "estado");
            var dt = ds.Tables[0]; //Variable data table arreglo de tablas

            foreach (DataRow row in dt.Rows)
            {
                var estados = new Estados
                {
                    Codigo = row["codigo"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    
                };

                list.Add(estados);
            }

            return list;
        }
    }


}

