﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Controlescolar
{
    public class Municipio
    {
        private int id;
        private string nombre;
        private string fk_estado;

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Fk_estado { get => fk_estado; set => fk_estado = value; }
    }
}
