﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Controlescolar
{
    public class Maestros
    {

        private string _numero_control;
        private string _nombre;
        private string _apellido_paterno;
        private string _apellido_materno;
        private string _fecha_nacimiento;
        private string _estado;
        private string _ciudad;
        private string _sexo;
        private string _correo;
        private string _tarjeta;
        private string _nivel_estudio;
        

        public string Numero_control { get => _numero_control; set => _numero_control = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Apellido_paterno { get => _apellido_paterno; set => _apellido_paterno = value; }
        public string Apellido_materno { get => _apellido_materno; set => _apellido_materno = value; }
        public string Fecha_nacimiento { get => _fecha_nacimiento; set => _fecha_nacimiento = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public string Ciudad { get => _ciudad; set => _ciudad = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string Correo { get => _correo; set => _correo = value; }
        public string Tarjeta { get => _tarjeta; set => _tarjeta = value; }
        public string Nivel_estudio { get => _nivel_estudio; set => _nivel_estudio = value; }
    }
}
