﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Controlescolar
{
    public class Alumnos
    { 
        private string numero_control;
        private string _nombre;
        private string _apellidoPaterno;
        private string _apellidoMaterno;
        private string _sexo;
        private string _fechanacimiento;
        private string _correo;
        private string _telefefono;
        private string _fkestado;
        private string _fkmunicipio;
        private string _domicilio;

        public string Numero_control { get => numero_control; set => numero_control = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string ApellidoPaterno { get => _apellidoPaterno; set => _apellidoPaterno = value; }
        public string ApellidoMaterno { get => _apellidoMaterno; set => _apellidoMaterno = value; }
        public string Sexo { get => _sexo; set => _sexo = value; }
        public string Fechanacimiento { get => _fechanacimiento; set => _fechanacimiento = value; }
        public string Correo { get => _correo; set => _correo = value; }
        public string Telefefono { get => _telefefono; set => _telefefono = value; }
        public string Fkestado { get => _fkestado; set => _fkestado = value; }
        public string Fkmunicipio { get => _fkmunicipio; set => _fkmunicipio = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
    }
    
}
