﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Controlescolar
{
    public class Escuela
    {
        private string _NombreEscuela;
        private string _DomicilioEscuela;
        private string _NumeroExterior;
        private string _Estado;
        private string _Municipio;
        private string _Telefono;
        private string _Pagina_Web;
        private string _email;
        private string _director;

        public string NombreEscuela { get => _NombreEscuela; set => _NombreEscuela = value; }
        public string DomicilioEscuela { get => _DomicilioEscuela; set => _DomicilioEscuela = value; }
        public string NumeroExterior { get => _NumeroExterior; set => _NumeroExterior = value; }
        public string Estado { get => _Estado; set => _Estado = value; }
        public string Municipio { get => _Municipio; set => _Municipio = value; }
        public string Telefono { get => _Telefono; set => _Telefono = value; }
        public string Pagina_Web { get => _Pagina_Web; set => _Pagina_Web = value; }
        public string Email { get => _email; set => _email = value; }
        public string Director { get => _director; set => _director = value; }
    }
}
