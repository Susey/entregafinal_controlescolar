﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.Controlescolar;
using Entidades.Controlescolar;


namespace Controlescolar
{
    public partial class Form1 : Form
    {
        UsuarioManejador _usuarioManejador;
        Usuarios _usuarios;
        public Form1()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuarios = new Usuarios(); //instancia
        }

        private void DgvUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void buscarUsuarios(string filtro)
        {
            dgvUsuarios.DataSource = _usuarioManejador.ObtenerLista(filtro);
        }
        private void EsValidarUusario(string filtro)
        {
            dgvUsuarios.DataSource = _usuarioManejador.ObtenerLista(filtro);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            buscarUsuarios("");
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscarUsuarios(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar este registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    buscarUsuarios("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void Eliminar()
        {
            int id = Convert.ToInt32(dgvUsuarios.CurrentRow.Cells["idusuario"].Value);
            _usuarioManejador.Eliminar(id);

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            UsuariosModal usuariosModal = new UsuariosModal();
            usuariosModal.ShowDialog();
            buscarUsuarios("");
        }

        private void DgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingUsuario();
            UsuariosModal usuariosModal = new UsuariosModal(_usuarios);
            usuariosModal.ShowDialog();
            buscarUsuarios("");

        }

        private void BindingUsuario()
        {
            _usuarios.IdUsuario = Convert.ToInt32(dgvUsuarios.CurrentRow.Cells["idusuario"].Value);
            _usuarios.Nombre = dgvUsuarios.CurrentRow.Cells["nombre"].Value.ToString();
            _usuarios.ApellidoPaterno = dgvUsuarios.CurrentRow.Cells["apellidopaterno"].Value.ToString();
            _usuarios.ApellidoMaterno = dgvUsuarios.CurrentRow.Cells["apellidomaterno"].Value.ToString();

        }
    }
}
