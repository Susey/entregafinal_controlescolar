﻿namespace Controlescolar
{
    partial class FrmMaestros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.Txt_Ciudad = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Cmb_Estado = new System.Windows.Forms.ComboBox();
            this.Date_Nacimiento = new System.Windows.Forms.DateTimePicker();
            this.Cmb_Sexo = new System.Windows.Forms.ComboBox();
            this.Txt_Nombre = new System.Windows.Forms.TextBox();
            this.Txt_ApellidoP = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.Txt_Correo = new System.Windows.Forms.TextBox();
            this.Txt_Tarjeta = new System.Windows.Forms.TextBox();
            this.Txt_NumControl = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Gabriola", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(490, 174);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(117, 50);
            this.button2.TabIndex = 49;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Gabriola", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(490, 90);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 48);
            this.button1.TabIndex = 48;
            this.button1.Text = "Guardar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Txt_Ciudad
            // 
            this.Txt_Ciudad.Location = new System.Drawing.Point(148, 224);
            this.Txt_Ciudad.Multiline = true;
            this.Txt_Ciudad.Name = "Txt_Ciudad";
            this.Txt_Ciudad.Size = new System.Drawing.Size(225, 26);
            this.Txt_Ciudad.TabIndex = 47;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(84, 217);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 35);
            this.label11.TabIndex = 46;
            this.label11.Text = "Ciudad:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(87, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 35);
            this.label9.TabIndex = 43;
            this.label9.Text = "Estado:";
            // 
            // Cmb_Estado
            // 
            this.Cmb_Estado.FormattingEnabled = true;
            this.Cmb_Estado.Location = new System.Drawing.Point(148, 191);
            this.Cmb_Estado.Name = "Cmb_Estado";
            this.Cmb_Estado.Size = new System.Drawing.Size(131, 21);
            this.Cmb_Estado.TabIndex = 42;
            // 
            // Date_Nacimiento
            // 
            this.Date_Nacimiento.Location = new System.Drawing.Point(148, 159);
            this.Date_Nacimiento.Name = "Date_Nacimiento";
            this.Date_Nacimiento.Size = new System.Drawing.Size(200, 20);
            this.Date_Nacimiento.TabIndex = 41;
            // 
            // Cmb_Sexo
            // 
            this.Cmb_Sexo.FormattingEnabled = true;
            this.Cmb_Sexo.Items.AddRange(new object[] {
            "Femenino",
            "Masculino"});
            this.Cmb_Sexo.Location = new System.Drawing.Point(148, 256);
            this.Cmb_Sexo.Name = "Cmb_Sexo";
            this.Cmb_Sexo.Size = new System.Drawing.Size(75, 21);
            this.Cmb_Sexo.TabIndex = 40;
            // 
            // Txt_Nombre
            // 
            this.Txt_Nombre.Location = new System.Drawing.Point(148, 52);
            this.Txt_Nombre.Multiline = true;
            this.Txt_Nombre.Name = "Txt_Nombre";
            this.Txt_Nombre.Size = new System.Drawing.Size(225, 26);
            this.Txt_Nombre.TabIndex = 39;
            // 
            // Txt_ApellidoP
            // 
            this.Txt_ApellidoP.Location = new System.Drawing.Point(148, 90);
            this.Txt_ApellidoP.Multiline = true;
            this.Txt_ApellidoP.Name = "Txt_ApellidoP";
            this.Txt_ApellidoP.Size = new System.Drawing.Size(225, 26);
            this.Txt_ApellidoP.TabIndex = 38;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(148, 125);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(225, 26);
            this.textBox6.TabIndex = 37;
            // 
            // Txt_Correo
            // 
            this.Txt_Correo.Location = new System.Drawing.Point(148, 285);
            this.Txt_Correo.Multiline = true;
            this.Txt_Correo.Name = "Txt_Correo";
            this.Txt_Correo.Size = new System.Drawing.Size(225, 26);
            this.Txt_Correo.TabIndex = 36;
            // 
            // Txt_Tarjeta
            // 
            this.Txt_Tarjeta.Location = new System.Drawing.Point(148, 317);
            this.Txt_Tarjeta.Multiline = true;
            this.Txt_Tarjeta.Name = "Txt_Tarjeta";
            this.Txt_Tarjeta.Size = new System.Drawing.Size(142, 26);
            this.Txt_Tarjeta.TabIndex = 35;
            // 
            // Txt_NumControl
            // 
            this.Txt_NumControl.Location = new System.Drawing.Point(148, 14);
            this.Txt_NumControl.Multiline = true;
            this.Txt_NumControl.Name = "Txt_NumControl";
            this.Txt_NumControl.Size = new System.Drawing.Size(142, 26);
            this.Txt_NumControl.TabIndex = 34;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(84, 308);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 35);
            this.label8.TabIndex = 33;
            this.label8.Text = "Tarjeta:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(93, 279);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 35);
            this.label7.TabIndex = 32;
            this.label7.Text = "Correo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 35);
            this.label6.TabIndex = 31;
            this.label6.Text = "Fecha de Nacimiento:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(97, 247);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 35);
            this.label5.TabIndex = 30;
            this.label5.Text = "Sexo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(29, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 35);
            this.label4.TabIndex = 29;
            this.label4.Text = "Apellido Materno:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(84, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 35);
            this.label3.TabIndex = 28;
            this.label3.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(34, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 35);
            this.label2.TabIndex = 27;
            this.label2.Text = "Apellido Paterno:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 35);
            this.label1.TabIndex = 26;
            this.label1.Text = "Numero de control:";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(148, 359);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(200, 40);
            this.button3.TabIndex = 50;
            this.button3.Text = "Agregar Nivel de estudios";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // FrmMaestros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 411);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Txt_Ciudad);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Cmb_Estado);
            this.Controls.Add(this.Date_Nacimiento);
            this.Controls.Add(this.Cmb_Sexo);
            this.Controls.Add(this.Txt_Nombre);
            this.Controls.Add(this.Txt_ApellidoP);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.Txt_Correo);
            this.Controls.Add(this.Txt_Tarjeta);
            this.Controls.Add(this.Txt_NumControl);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmMaestros";
            this.Text = "FrmMaestros";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox Txt_Ciudad;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox Cmb_Estado;
        private System.Windows.Forms.DateTimePicker Date_Nacimiento;
        private System.Windows.Forms.ComboBox Cmb_Sexo;
        private System.Windows.Forms.TextBox Txt_Nombre;
        private System.Windows.Forms.TextBox Txt_ApellidoP;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox Txt_Correo;
        private System.Windows.Forms.TextBox Txt_Tarjeta;
        private System.Windows.Forms.TextBox Txt_NumControl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
    }
}