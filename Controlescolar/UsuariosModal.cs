﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.Controlescolar;
using Entidades.Controlescolar;

namespace Controlescolar
{
    public partial class UsuariosModal : Form
    {
        private UsuarioManejador _usuarioManejador;
        private Usuarios _usuario;
        private bool _isEnableBinding = false;

        public UsuariosModal()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuario = new Usuarios();
            _isEnableBinding = true;
            //BindingUsuario(); //Actualiza datos 
        }

        public UsuariosModal(Usuarios usuario)
        {
            //Se esta haciendo otro constructor para mandar llamar los datos que se estan
            //agregando y guardando los mande al datagrid automaticamente
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuario = new Usuarios();
            _usuario = usuario;
            BindingUsuarioReload();
            _isEnableBinding = true; //habilitar las variables 

        }
        private void UsuariosModal_Load(object sender, EventArgs e)
        {

        }

        private void BindingUsuarioReload()
        {
            textBox1.Text = _usuario.Nombre;
            textBox2.Text = _usuario.ApellidoPaterno;
            textBox3.Text = _usuario.ApellidoMaterno;
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            BindingUsuario();
            if (ValidarUsuario())
            {
                Guardar();
                this.Close();
            }
        }
        private bool ValidarUsuario()
        {
            var res = _usuarioManejador.EsusuarioValidoU(_usuario);
            if (!res.Item1)
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }
        private void Guardar()
        {
            _usuarioManejador.Guardar(_usuario);

        }
        private void BindingUsuario()
        {
            if (_isEnableBinding)
            {
                if (_usuario.IdUsuario == 0)
                {
                    _usuario.IdUsuario = 0;
                }

                _usuario.Nombre = textBox1.Text;
                _usuario.ApellidoPaterno = textBox2.Text;
                _usuario.ApellidoMaterno = textBox3.Text;
            }

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            BindingUsuario();
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            BindingUsuario();
        }

        private void TextBox3_TextChanged(object sender, EventArgs e)
        {
            BindingUsuario();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
