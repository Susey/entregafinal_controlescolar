﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.Controlescolar;
using Entidades.Controlescolar;

namespace Controlescolar
{
    public partial class FrmAlumnos : Form
    {
        private AlumnosManejador _alumnoManejador;
        private EstadosManejador _estadosManejador;
        private MunicipioManejador _municipioManejador;
        private Alumnos _alumno;
        private Estados _estado;
        private Municipio _municipio;
        private bool _isEnableBinding = false;

        public FrmAlumnos()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnosManejador();
            _estadosManejador = new EstadosManejador();
            _municipioManejador = new MunicipioManejador();
            _alumno = new Alumnos();
            _estado = new Estados();
            _municipio = new Municipio();
            _isEnableBinding = true;
        }
        private void BindingAlumnosReload()
        {
            Txt_Numcontrol.Text = _alumno.Numero_control;
            Txt_NombreAlumno.Text = _alumno.Nombre;
            Txt_Apellidop.Text = _alumno.ApellidoPaterno;
            Txt_Apellidom.Text = _alumno.ApellidoMaterno;
            Cmb_Sexo.Text = _alumno.Sexo;
            Dtp_Fechan.Text = _alumno.Fechanacimiento;
            Txt_correo.Text = _alumno.Correo;
            Txt_Telefono.Text = _alumno.Telefefono;
            Cmb_Estado.Text = _alumno.Fkestado;
            Cmb_Municipio.Text = _alumno.Fkmunicipio;
            Txt_Domicilio.Text = _alumno.Domicilio;
        }
        public FrmAlumnos(Alumnos alumno)
        {
           
            InitializeComponent();
            _alumnoManejador = new AlumnosManejador();
            _estadosManejador = new EstadosManejador();
            _municipioManejador = new MunicipioManejador();
            _alumno = new Alumnos();
            /*_estado = new Estados();
            _estado = estado;
            _municipio = new Municipio();
            _municipio = municipio;*/
            BindingAlumnosReload();
            _isEnableBinding = true; //habilitar las variables 
        }
        private bool esUsuarioValido()
        {
            var res = _alumnoManejador.EsusuarioValidoA(_alumno);
            if (!res.Item1)
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }
        private void Guardar()
        {
            _alumnoManejador.Guardar(_alumno);

        }
        private void BindingAlumnos()
        {
            if (_isEnableBinding)
            {
                if (_alumno.Numero_control != "")
                {
                    _alumno.Numero_control = "";
                }

                _alumno.Numero_control = Txt_Numcontrol.Text;
                _alumno.Nombre = Txt_NombreAlumno.Text;
                _alumno.ApellidoPaterno = Txt_Apellidop.Text;
                _alumno.ApellidoMaterno = Txt_Apellidop.Text;
                _alumno.Sexo = Cmb_Sexo.Text;
                _alumno.Fechanacimiento = Dtp_Fechan.Text;
                _alumno.Correo = Txt_correo.Text;
                _alumno.Telefefono = Txt_Telefono.Text;
                _alumno.Fkestado = Cmb_Estado.Text;
                _alumno.Fkmunicipio = Cmb_Municipio.Text;
                _alumno.Domicilio = Txt_Domicilio.Text;

            }

        }
        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            BindingAlumnos();
            if (esUsuarioValido())
            { 
                Guardar();
                this.Close();
            }
        }

        private void FrmAlumnos_Load(object sender, EventArgs e)
        {
            var Estados = _estadosManejador.ObtenerLista("");
        }

        private void Cmb_Estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingAlumnos();
            Cmb_Estado.Items.Clear();
            string filtro = "";
            var Estados = _estadosManejador.ObtenerLista("");
            foreach (var item in Estados)
            {
                if (item.Nombre.Equals(Cmb_Estado.Text))
                {
                    filtro = item.Codigo;
                }
            }
            var Municipios = _municipioManejador.ObtenerLista(filtro);
            foreach (var municipio in Municipios)
            {
                Cmb_Estado.Items.Add(municipio.Nombre);
            }

        }
        
    }
}