﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.Controlescolar;
using Entidades.Controlescolar;

namespace Controlescolar
{
    public partial class FrmAlumnos1 : Form
    {
        AlumnosManejador _alumnosManejador;
        Alumnos _alumno;
        public FrmAlumnos1()
        {
            InitializeComponent();
            _alumnosManejador = new AlumnosManejador();
            _alumno = new Alumnos();
        }
        private void BuscarAlumnos(string filtro)
        {
            dgvalumnos.DataSource = _alumnosManejador.ObtenerLista(filtro);
        }
        private void FrmAlumnos1_Load(object sender, EventArgs e)
        {
            BuscarAlumnos("");
        }
        private void Eliminar()
        {
            string id = dgvalumnos.CurrentRow.Cells["numero_control"].Value.ToString();
            _alumnosManejador.Eliminar(id);
        }
        private void BindingAlumno()
        {
            _alumno.Numero_control = dgvalumnos.CurrentRow.Cells["numero_control"].Value.ToString();
            _alumno.Nombre = dgvalumnos.CurrentRow.Cells["nombre"].Value.ToString();
            _alumno.ApellidoPaterno = dgvalumnos.CurrentRow.Cells["apellido_Paterno"].Value.ToString();
            _alumno.ApellidoMaterno = dgvalumnos.CurrentRow.Cells["apellido_Materno"].Value.ToString();
            _alumno.Sexo = dgvalumnos.CurrentRow.Cells["sexo"].Value.ToString();
            _alumno.Fechanacimiento = dgvalumnos.CurrentRow.Cells["fechaNacimiento"].Value.ToString();
            _alumno.Correo = dgvalumnos.CurrentRow.Cells["correo"].Value.ToString();
            _alumno.Telefefono = dgvalumnos.CurrentRow.Cells["telefefono"].Value.ToString();
            _alumno.Fkestado = dgvalumnos.CurrentRow.Cells["fkestado"].Value.ToString();
            _alumno.Fkmunicipio = dgvalumnos.CurrentRow.Cells["fkmunicipio"].Value.ToString();
            _alumno.Domicilio = dgvalumnos.CurrentRow.Cells["domicilio"].Value.ToString();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            FrmAlumnos frmAlumnos = new FrmAlumnos();
            frmAlumnos.ShowDialog();
            BuscarAlumnos("");
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("¿Estas seguro que deseas eliminar resistro?", "Eliminar Registro",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    BuscarAlumnos("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dgvalumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingAlumno();
            FrmAlumnos frmAlumnos = new FrmAlumnos(_alumno);
            frmAlumnos.ShowDialog();
            BuscarAlumnos("");
        }
    }
}
