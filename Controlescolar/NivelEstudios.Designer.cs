﻿namespace Controlescolar
{
    partial class NivelEstudios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnCancelar = new System.Windows.Forms.Button();
            this.BtnGuardar = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.AddDoctorado = new System.Windows.Forms.Button();
            this.AddMaestria = new System.Windows.Forms.Button();
            this.AddLic = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.Txt_doctorado = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.Txt_Maestria = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_Lic = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Font = new System.Drawing.Font("Gabriola", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCancelar.Location = new System.Drawing.Point(619, 116);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(117, 42);
            this.BtnCancelar.TabIndex = 79;
            this.BtnCancelar.Text = "Cancelar";
            this.BtnCancelar.UseVisualStyleBackColor = true;
            this.BtnCancelar.Click += new System.EventHandler(this.BtnCancelar_Click);
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Font = new System.Drawing.Font("Gabriola", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGuardar.Location = new System.Drawing.Point(619, 62);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Size = new System.Drawing.Size(117, 42);
            this.BtnGuardar.TabIndex = 78;
            this.BtnGuardar.Text = "Guardar";
            this.BtnGuardar.UseVisualStyleBackColor = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Gabriola", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(465, 138);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(117, 42);
            this.button8.TabIndex = 77;
            this.button8.Text = "Limpiar";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Gabriola", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(465, 86);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(117, 42);
            this.button7.TabIndex = 76;
            this.button7.Text = "Limpiar";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Gabriola", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(465, 38);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(117, 42);
            this.button6.TabIndex = 75;
            this.button6.Text = "Limpiar";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // AddDoctorado
            // 
            this.AddDoctorado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddDoctorado.Location = new System.Drawing.Point(399, 127);
            this.AddDoctorado.Name = "AddDoctorado";
            this.AddDoctorado.Size = new System.Drawing.Size(39, 23);
            this.AddDoctorado.TabIndex = 74;
            this.AddDoctorado.Text = "...";
            this.AddDoctorado.UseVisualStyleBackColor = true;
            this.AddDoctorado.Click += new System.EventHandler(this.AddDoctorado_Click);
            // 
            // AddMaestria
            // 
            this.AddMaestria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddMaestria.Location = new System.Drawing.Point(399, 92);
            this.AddMaestria.Name = "AddMaestria";
            this.AddMaestria.Size = new System.Drawing.Size(39, 23);
            this.AddMaestria.TabIndex = 73;
            this.AddMaestria.Text = "...";
            this.AddMaestria.UseVisualStyleBackColor = true;
            this.AddMaestria.Click += new System.EventHandler(this.AddMaestria_Click);
            // 
            // AddLic
            // 
            this.AddLic.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddLic.Location = new System.Drawing.Point(399, 57);
            this.AddLic.Name = "AddLic";
            this.AddLic.Size = new System.Drawing.Size(39, 26);
            this.AddLic.TabIndex = 72;
            this.AddLic.Text = "...";
            this.AddLic.UseVisualStyleBackColor = true;
            this.AddLic.Click += new System.EventHandler(this.AddLic_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(143, 35);
            this.label14.TabIndex = 71;
            this.label14.Text = "NIVEL DE ESTUDIOS";
            // 
            // Txt_doctorado
            // 
            this.Txt_doctorado.Location = new System.Drawing.Point(251, 124);
            this.Txt_doctorado.Multiline = true;
            this.Txt_doctorado.Name = "Txt_doctorado";
            this.Txt_doctorado.Size = new System.Drawing.Size(142, 26);
            this.Txt_doctorado.TabIndex = 70;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(158, 115);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 35);
            this.label13.TabIndex = 69;
            this.label13.Text = "Doctorado:";
            // 
            // Txt_Maestria
            // 
            this.Txt_Maestria.Location = new System.Drawing.Point(251, 92);
            this.Txt_Maestria.Multiline = true;
            this.Txt_Maestria.Name = "Txt_Maestria";
            this.Txt_Maestria.Size = new System.Drawing.Size(142, 26);
            this.Txt_Maestria.TabIndex = 68;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(169, 83);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 35);
            this.label12.TabIndex = 67;
            this.label12.Text = "Maestria:";
            // 
            // Txt_Lic
            // 
            this.Txt_Lic.Location = new System.Drawing.Point(251, 57);
            this.Txt_Lic.Multiline = true;
            this.Txt_Lic.Name = "Txt_Lic";
            this.Txt_Lic.Size = new System.Drawing.Size(142, 26);
            this.Txt_Lic.TabIndex = 66;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(73, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(166, 35);
            this.label10.TabIndex = 65;
            this.label10.Text = "Licenciatura o Ingeneiría:";
            // 
            // NivelEstudios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 218);
            this.Controls.Add(this.BtnCancelar);
            this.Controls.Add(this.BtnGuardar);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.AddDoctorado);
            this.Controls.Add(this.AddMaestria);
            this.Controls.Add(this.AddLic);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Txt_doctorado);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Txt_Maestria);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.Txt_Lic);
            this.Controls.Add(this.label10);
            this.Name = "NivelEstudios";
            this.Text = "NivelEstudios";
            this.Load += new System.EventHandler(this.NivelEstudios_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnCancelar;
        private System.Windows.Forms.Button BtnGuardar;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button AddDoctorado;
        private System.Windows.Forms.Button AddMaestria;
        private System.Windows.Forms.Button AddLic;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Txt_doctorado;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox Txt_Maestria;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Txt_Lic;
        private System.Windows.Forms.Label label10;
    }
}