﻿namespace Controlescolar
{
    partial class FrmAlumnos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_Numcontrol = new System.Windows.Forms.TextBox();
            this.Txt_Telefono = new System.Windows.Forms.TextBox();
            this.Txt_correo = new System.Windows.Forms.TextBox();
            this.Txt_Apellidom = new System.Windows.Forms.TextBox();
            this.Txt_Apellidop = new System.Windows.Forms.TextBox();
            this.Txt_NombreAlumno = new System.Windows.Forms.TextBox();
            this.Cmb_Sexo = new System.Windows.Forms.ComboBox();
            this.Dtp_Fechan = new System.Windows.Forms.DateTimePicker();
            this.Cmb_Estado = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Cmb_Municipio = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_Domicilio = new System.Windows.Forms.TextBox();
            this.Btn_Guardar = new System.Windows.Forms.Button();
            this.Btn_Cancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero de control:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 35);
            this.label2.TabIndex = 1;
            this.label2.Text = "Apellido Paterno:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(86, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 35);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombre:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(31, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 35);
            this.label4.TabIndex = 3;
            this.label4.Text = "Apellido Materno:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(108, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 35);
            this.label5.TabIndex = 4;
            this.label5.Text = "Sexo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 192);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 35);
            this.label6.TabIndex = 5;
            this.label6.Text = "Fecha de Nacimiento:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(95, 227);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 35);
            this.label7.TabIndex = 6;
            this.label7.Text = "Correo:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(86, 262);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 35);
            this.label8.TabIndex = 7;
            this.label8.Text = "Teléfono:";
            // 
            // Txt_Numcontrol
            // 
            this.Txt_Numcontrol.Location = new System.Drawing.Point(150, 17);
            this.Txt_Numcontrol.Multiline = true;
            this.Txt_Numcontrol.Name = "Txt_Numcontrol";
            this.Txt_Numcontrol.Size = new System.Drawing.Size(142, 26);
            this.Txt_Numcontrol.TabIndex = 8;
            // 
            // Txt_Telefono
            // 
            this.Txt_Telefono.Location = new System.Drawing.Point(150, 271);
            this.Txt_Telefono.Multiline = true;
            this.Txt_Telefono.Name = "Txt_Telefono";
            this.Txt_Telefono.Size = new System.Drawing.Size(142, 26);
            this.Txt_Telefono.TabIndex = 10;
            // 
            // Txt_correo
            // 
            this.Txt_correo.Location = new System.Drawing.Point(150, 233);
            this.Txt_correo.Multiline = true;
            this.Txt_correo.Name = "Txt_correo";
            this.Txt_correo.Size = new System.Drawing.Size(225, 26);
            this.Txt_correo.TabIndex = 11;
            // 
            // Txt_Apellidom
            // 
            this.Txt_Apellidom.Location = new System.Drawing.Point(150, 128);
            this.Txt_Apellidom.Multiline = true;
            this.Txt_Apellidom.Name = "Txt_Apellidom";
            this.Txt_Apellidom.Size = new System.Drawing.Size(225, 26);
            this.Txt_Apellidom.TabIndex = 13;
            // 
            // Txt_Apellidop
            // 
            this.Txt_Apellidop.Location = new System.Drawing.Point(150, 93);
            this.Txt_Apellidop.Multiline = true;
            this.Txt_Apellidop.Name = "Txt_Apellidop";
            this.Txt_Apellidop.Size = new System.Drawing.Size(225, 26);
            this.Txt_Apellidop.TabIndex = 14;
            // 
            // Txt_NombreAlumno
            // 
            this.Txt_NombreAlumno.Location = new System.Drawing.Point(150, 55);
            this.Txt_NombreAlumno.Multiline = true;
            this.Txt_NombreAlumno.Name = "Txt_NombreAlumno";
            this.Txt_NombreAlumno.Size = new System.Drawing.Size(225, 26);
            this.Txt_NombreAlumno.TabIndex = 15;
            // 
            // Cmb_Sexo
            // 
            this.Cmb_Sexo.FormattingEnabled = true;
            this.Cmb_Sexo.Items.AddRange(new object[] {
            "Femenino",
            "Masculino"});
            this.Cmb_Sexo.Location = new System.Drawing.Point(150, 161);
            this.Cmb_Sexo.Name = "Cmb_Sexo";
            this.Cmb_Sexo.Size = new System.Drawing.Size(75, 21);
            this.Cmb_Sexo.TabIndex = 16;
            // 
            // Dtp_Fechan
            // 
            this.Dtp_Fechan.Location = new System.Drawing.Point(150, 196);
            this.Dtp_Fechan.Name = "Dtp_Fechan";
            this.Dtp_Fechan.Size = new System.Drawing.Size(200, 20);
            this.Dtp_Fechan.TabIndex = 17;
            // 
            // Cmb_Estado
            // 
            this.Cmb_Estado.FormattingEnabled = true;
            this.Cmb_Estado.Location = new System.Drawing.Point(150, 317);
            this.Cmb_Estado.Name = "Cmb_Estado";
            this.Cmb_Estado.Size = new System.Drawing.Size(131, 21);
            this.Cmb_Estado.TabIndex = 18;
            this.Cmb_Estado.SelectedIndexChanged += new System.EventHandler(this.Cmb_Estado_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(85, 308);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 35);
            this.label9.TabIndex = 19;
            this.label9.Text = "Estado:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(65, 343);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 35);
            this.label10.TabIndex = 20;
            this.label10.Text = "Municipio:";
            // 
            // Cmb_Municipio
            // 
            this.Cmb_Municipio.FormattingEnabled = true;
            this.Cmb_Municipio.Location = new System.Drawing.Point(150, 352);
            this.Cmb_Municipio.Name = "Cmb_Municipio";
            this.Cmb_Municipio.Size = new System.Drawing.Size(131, 21);
            this.Cmb_Municipio.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(65, 378);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 35);
            this.label11.TabIndex = 22;
            this.label11.Text = "Domicilio:";
            // 
            // Txt_Domicilio
            // 
            this.Txt_Domicilio.Location = new System.Drawing.Point(150, 387);
            this.Txt_Domicilio.Multiline = true;
            this.Txt_Domicilio.Name = "Txt_Domicilio";
            this.Txt_Domicilio.Size = new System.Drawing.Size(225, 26);
            this.Txt_Domicilio.TabIndex = 23;
            // 
            // Btn_Guardar
            // 
            this.Btn_Guardar.Font = new System.Drawing.Font("Gabriola", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Guardar.Location = new System.Drawing.Point(503, 157);
            this.Btn_Guardar.Name = "Btn_Guardar";
            this.Btn_Guardar.Size = new System.Drawing.Size(117, 42);
            this.Btn_Guardar.TabIndex = 24;
            this.Btn_Guardar.Text = "Guardar";
            this.Btn_Guardar.UseVisualStyleBackColor = true;
            this.Btn_Guardar.Click += new System.EventHandler(this.Btn_Guardar_Click);
            // 
            // Btn_Cancelar
            // 
            this.Btn_Cancelar.Font = new System.Drawing.Font("Gabriola", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Cancelar.Location = new System.Drawing.Point(503, 262);
            this.Btn_Cancelar.Name = "Btn_Cancelar";
            this.Btn_Cancelar.Size = new System.Drawing.Size(117, 42);
            this.Btn_Cancelar.TabIndex = 25;
            this.Btn_Cancelar.Text = "Cancelar";
            this.Btn_Cancelar.UseVisualStyleBackColor = true;
            // 
            // FrmAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 469);
            this.Controls.Add(this.Btn_Cancelar);
            this.Controls.Add(this.Btn_Guardar);
            this.Controls.Add(this.Txt_Domicilio);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.Cmb_Municipio);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Cmb_Estado);
            this.Controls.Add(this.Dtp_Fechan);
            this.Controls.Add(this.Cmb_Sexo);
            this.Controls.Add(this.Txt_NombreAlumno);
            this.Controls.Add(this.Txt_Apellidop);
            this.Controls.Add(this.Txt_Apellidom);
            this.Controls.Add(this.Txt_correo);
            this.Controls.Add(this.Txt_Telefono);
            this.Controls.Add(this.Txt_Numcontrol);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmAlumnos";
            this.Text = "FrmAlumnos";
            this.Load += new System.EventHandler(this.FrmAlumnos_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_Numcontrol;
        private System.Windows.Forms.TextBox Txt_Telefono;
        private System.Windows.Forms.TextBox Txt_correo;
        private System.Windows.Forms.TextBox Txt_Apellidom;
        private System.Windows.Forms.TextBox Txt_Apellidop;
        private System.Windows.Forms.TextBox Txt_NombreAlumno;
        private System.Windows.Forms.ComboBox Cmb_Sexo;
        private System.Windows.Forms.DateTimePicker Dtp_Fechan;
        private System.Windows.Forms.ComboBox Cmb_Estado;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox Cmb_Municipio;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_Domicilio;
        private System.Windows.Forms.Button Btn_Guardar;
        private System.Windows.Forms.Button Btn_Cancelar;
    }
}