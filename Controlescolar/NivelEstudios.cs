﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Controlescolar
{
    public partial class NivelEstudios : Form
    {
        private OpenFileDialog _Archivos;
        private string _rutaarchivos;

        public NivelEstudios()
        {
            InitializeComponent();
            _Archivos = new OpenFileDialog();
            _rutaarchivos = Application.StartupPath + "\\LD2019\\";
        }

        private void NivelEstudios_Load(object sender, EventArgs e)
        {

        }
        private void AddLic_Click(object sender, EventArgs e)
        {
            CargarArchivo();
        }
        private void CargarArchivo()
        {
            _Archivos.Filter = "Archivo tipo (*.jpg)|.jpg(*.pdf)*.pdf";
            _Archivos.Title = "cargar archivo";
            _Archivos.ShowDialog();

            if (_Archivos.FileName != "")
            {
                var arch = new FileInfo(_Archivos.FileName);

                Txt_Lic.Text = arch.Name;

            }
        }

        private void AddMaestria_Click(object sender, EventArgs e)
        {
            CargarArchivo();
        }

        private void AddDoctorado_Click(object sender, EventArgs e)
        {
            CargarArchivo();
        }
        private void GuardarArchivos()
        {
            if (_Archivos.FileName != null)
            {
                if (_Archivos.FileName != "")
                {
                    var archivo = new FileInfo(_Archivos.FileName);
                    if (Directory.Exists(_rutaarchivos))
                    {
                        //codigo paera agregar archivo
                        var obtenerArchivos = Directory.GetFiles(_rutaarchivos, "*.jpg,*.pdf");

                        FileInfo archivoAnterior;
                        if (obtenerArchivos.Length != 0)
                        {
                            //codigo para remplazar archivo
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_rutaarchivos + archivo.Name);
                            }

                        }
                        else
                        {
                            archivo.CopyTo(_rutaarchivos + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_rutaarchivos);
                        archivo.CopyTo(_rutaarchivos + archivo.Name);
                    }
                }
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            GuardarArchivos();
        }
        private void EliminarArchivos()
        {
            if (MessageBox.Show("Estas seguro que deseas eliminar Pdf, Jpg", "Eliminar pdf, Jpg", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_rutaarchivos))
                {
                    //codigo paera agregar archivo
                    var obtenerArchivos = Directory.GetFiles(_rutaarchivos, "*.jpg,*.pdf");

                    FileInfo archivoAnterior;
                    if (obtenerArchivos.Length != 0)
                    {
                        //codigo para remplazar imagen
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();

                        }

                    }
                }
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            EliminarArchivos();
        }
        
    }
}
