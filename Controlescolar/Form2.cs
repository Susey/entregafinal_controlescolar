﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controlescolar
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void CatalogosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 frmUsuarios = new Form1();
            frmUsuarios.ShowDialog();
          
        }

        private void AlumnosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAlumnos1 frmAlumnos1 = new FrmAlumnos1();
            frmAlumnos1.ShowDialog();
        }
    }
    }
